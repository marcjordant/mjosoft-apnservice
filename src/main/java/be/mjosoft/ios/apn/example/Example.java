package be.mjosoft.ios.apn.example;

import be.mjosoft.ios.apn.APNException;
import be.mjosoft.ios.apn.APNFeedBackService;
import be.mjosoft.ios.apn.APNMessage;
import be.mjosoft.ios.apn.APNService;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Example class.
 * @author mj
 */
public class Example
{
	public static void main(String args[]) throws APNException
	{
		sendNotifications();
		checkFeedback();
	}

	/**
	 * Check the feedback service.
	 * @throws APNException
	 */
	public static void checkFeedback() throws APNException
	{
		APNFeedBackService service = new APNFeedBackService();
		service.initConnexion("<path to cert.p12>", "<password>", "PKCS12");
		List<APNFeedBackService.ObsoleteDevice> devices = service.retreiveObsoleteDevices();
		service.endConnection();

		for(APNFeedBackService.ObsoleteDevice device : devices){
			System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(device.getTimestamp().getTime()) + " > " + device.getToken());
		}
	}

	/**
	 * Sends notifications.
	 * @throws APNException
	 */
	public static void sendNotifications() throws APNException
	{
		// Create the service.
		APNService service = new APNService();
		service.initConnexion("<path to cert.p12>", "<password>", "PKCS12");

		// Create a simple notification.
		APNMessage msgA = new APNMessage("Alert message", 1, "default");
		System.out.println(msgA);
		// Send the notification
		service.sendAPN("<my device token>", msgA);

		// Increments the badge value.
		msgA.incrementBadge(2);
		// Send the notification
		service.sendAPN("<my device token>", msgA);

		// Create a complexe notification.
		APNMessage msgB = new APNMessage();
		APNMessage.CustomAlert customAlert = new APNMessage.CustomAlert();
		customAlert.setBody("Another message");
		customAlert.setLocalizedActionKey("play");
		customAlert.setLocalizedMessageKey("message1.key");
		customAlert.setLocalizedMessageArguments("John", "Mary", "1");
		msgB.setCustomAlert(customAlert);
		System.out.println(msgB);
		// Send the notification.
		service.sendAPN("<my device token>", msgB);

		service.endConnection();
	}
}
