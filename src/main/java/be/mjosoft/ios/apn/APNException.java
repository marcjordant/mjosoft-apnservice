package be.mjosoft.ios.apn;

/**
 * Exception thrown by the APNService class.
 * @author mj
 */
public class APNException extends Exception
{

	public APNException(Throwable cause)
	{
		super(cause);
	}

	public APNException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public APNException(String message)
	{
		super(message);
	}

	public APNException()
	{
	}
}